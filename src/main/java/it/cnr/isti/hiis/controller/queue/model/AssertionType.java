package it.cnr.isti.hiis.controller.queue.model;

/**
 *
 * @author Marco Manca
 */
public enum AssertionType {
    error, cannotTell, pass
}
