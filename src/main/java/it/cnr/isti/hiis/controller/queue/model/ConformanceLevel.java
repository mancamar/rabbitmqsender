package it.cnr.isti.hiis.controller.queue.model;

/**
 *
 * @author Marco Manca
 */
public enum ConformanceLevel {
    A, AA, AAA
}
