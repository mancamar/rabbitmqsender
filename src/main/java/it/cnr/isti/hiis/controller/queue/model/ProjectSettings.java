package it.cnr.isti.hiis.controller.queue.model;

import java.util.List;
/**
 *
 * @author Marco Manca
 */
public class ProjectSettings {
    private Guideline guideline;
    private String method;
    private ConformanceLevel conformanceLevel;
    private int maxNumber;
    private List<String> exclusionCriteria;
    private int depth;

    public Guideline getGuideline() {
        return guideline;
    }

    public void setGuideline(Guideline guideline) {
        this.guideline = guideline;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public ConformanceLevel getConformanceLevel() {
        return conformanceLevel;
    }

    public void setConformanceLevel(ConformanceLevel conformanceLevel) {
        this.conformanceLevel = conformanceLevel;
    }

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public List<String> getExclusionCriteria() {
        return exclusionCriteria;
    }

    public void setExclusionCriteria(List<String> exclusionCriteria) {
        this.exclusionCriteria = exclusionCriteria;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
    
    
}
