package it.cnr.isti.hiis.controller.queue.model;

/**
 *
 * @author Marco Manca
 */
public enum JobType {
    evaluate, crawl, requestAssertions, getRuleset
}
