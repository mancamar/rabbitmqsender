package it.cnr.isti.hiis.controller.queue.model;

/**
 *
 * @author Marco Manca
 */
public enum Status {
    ok, done, error, fail
}
