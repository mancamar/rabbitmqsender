package it.cnr.isti.hiis.controller.queue.model;

/**
 *
 * @author Marco Manca
 */
public enum Guideline {    
    WCAG_20, WCAG_21
}
