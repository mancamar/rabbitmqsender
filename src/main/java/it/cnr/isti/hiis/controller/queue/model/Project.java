package it.cnr.isti.hiis.controller.queue.model;

import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class Project {
    private Integer projectID;
    private Integer ToolID;
    private List<String> contentType;
    private List<AssertionType> issueTypes;
    private String uri;

    public Project() {
    }

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    public Integer getToolID() {
        return ToolID;
    }

    public void setToolID(Integer ToolID) {
        this.ToolID = ToolID;
    }
    
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<String> getContentType() {
        return contentType;
    }

    public void setContentType(List<String> contentType) {
        this.contentType = contentType;
    }

    public List<AssertionType> getIssueTypes() {
        return issueTypes;
    }

    public void setIssueTypes(List<AssertionType> issueTypes) {
        this.issueTypes = issueTypes;
    }
    
    
}
