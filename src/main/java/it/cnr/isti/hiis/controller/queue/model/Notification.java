package it.cnr.isti.hiis.controller.queue.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class Notification {
    @JsonProperty("job-id")
    private Integer job_id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    List<String> assertions;
    Status status;
    List<String> ruleset;

    public Notification() {
    }

    public Notification(Integer job_id, List<String> assertions, Status status, List<String> ruleset) {
        this.job_id = job_id;
        this.assertions = assertions;
        this.status = status;
        this.ruleset = ruleset;
    }
    
    public Integer getJob_id() {
        return job_id;
    }

    public void setJob_id(Integer job_id) {
        this.job_id = job_id;
    }

    public List<String> getAssertions() {
        return assertions;
    }

    public void setAssertions(List<String> assertions) {
        this.assertions = assertions;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<String> getRuleset() {
        return ruleset;
    }

    public void setRuleset(List<String> ruleset) {
        this.ruleset = ruleset;
    }
    
    
}
