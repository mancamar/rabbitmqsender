package it.cnr.isti.hiis.rabbitmqsender;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import it.cnr.isti.hiis.controller.queue.model.JobRequest;
import it.cnr.isti.hiis.controller.queue.model.JobType;
import it.cnr.isti.hiis.controller.queue.model.Project;
import it.cnr.isti.hiis.controller.queue.model.ProjectSettings;

/**
 *
 * @author Marco Manca
 */
public class SendMessage {

    private final static String QUEUE_NAME = "jobQueue";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            //String message = "{ \"type\":\"crawl\", \"url\" : \"http://mauve.isti.cnr.it\"}";
            JobRequest req = new JobRequest();
            req.setJob_id(100);
            req.setAuditID("qwerty");
            req.setJobType(JobType.crawl);
            req.setTool("MAUVE");
            
            Project prj = new Project();
            prj.setProjectID(666);
            prj.setUri("http://pages.di.unipi.it/broccia/");
            req.setProject(prj);
            
            ProjectSettings settings = new ProjectSettings();
            settings.setMaxNumber(5);
            settings.setDepth(2);
            req.setProjectSettings(settings);
            
            ObjectMapper map = new ObjectMapper();
            channel.basicPublish("", QUEUE_NAME, null, map.writeValueAsBytes(req));
            System.out.println(" [x] Sent '" + map.writeValueAsString(req) + "'");
        }
    }
}
